<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_PushnotificationGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\PushnotificationGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlAuthenticationException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mobicommerce\Mobiapp\Model\DevicetokenFactory;
use Magento\Framework\Exception\LocalizedException;

class SaveDeviceToken implements ResolverInterface
{
    public function __construct(
        DevicetokenFactory $devicetokenFactory
    ) {
        $this->devicetokenFactory = $devicetokenFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (empty($args['token'])) {
            throw new GraphQlInputException(__('Specify the "token" value.'));
        }

        if (empty($args['deviceType'])) {
            throw new GraphQlInputException(__('Specify the "device_type" value.'));
        }

        try {
            $token = $args['token'];
            $deviceType = $args['deviceType'];
            $customerId = $args['customerId'] ?? null;
            if (!$customerId) {
                $customerId = null;
            }

            $collection = $this->devicetokenFactory->create()->getCollection();
            $collection->addFieldToFilter('token', $token)
                ->addFieldToFilter('device_type', $deviceType);

            if ($collection->getSize()) {
                foreach ($collection as $item) {
                    $item->setCustomerId($customerId)->save();
                }
            } else {
                $model = $this->devicetokenFactory->create();
                $model->setData([
                    'token' => $token,
                    'device_type' => $deviceType,
                    'customerId' => $customerId
                ])->save();
            }

            return true;
        } catch (LocalizedException $e) {
            throw new GraphQlAuthenticationException(__($e->getMessage()), $e);
        }
    }
}
